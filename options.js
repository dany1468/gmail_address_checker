window.onload = function() {
  restore_options();

  var saveButton = document.getElementById('button-save');
  saveButton.addEventListener('click', save_options);
};

function save_options() {
  var invalid_color = document.getElementById('invalid_color').value;
  var valid_color = document.getElementById('valid_color').value;

  var domain = document.getElementById('white_domain').value;
  localStorage["alert_settings"] =JSON.stringify({ "white_domain" : domain,  valid_color : valid_color,  invalid_color: invalid_color });
  
  var status = document.getElementById("status");
  status.innerHTML = "Options Saved.";
  setTimeout(function() {
    status.innerHTML = "";
  }, 1000);
}

function restore_options() {
  var settings = localStorage["alert_settings"];
  if (!settings) return;
  settings =JSON.parse(settings); 
  document.getElementById('invalid_color').value = settings.invalid_color || '';
  document.getElementById('valid_color').value = settings.valid_color || '';
  document.getElementById('white_domain').value = settings.white_domain || '';
}