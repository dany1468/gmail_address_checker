(function() {
  window.setTimeout(function() {

    // NOTE GMail の宛先の削除は x リンクで実行できるため、本来 blur だけでは補足できないが
    //      実装の簡略化のため宛先入力エリア (to or cc or bcc) の blur イベントのみにしている。
    document.addEventListener('blur', function(event) {
      var target = event.target;
      if (target.name !== 'to' && target.name !== 'cc' && target.name !== 'bcc') return;

      chrome.extension.sendRequest({method: "getLocalStorageData", key: "alert_settings"}, function(response) {
        if (!response.result) return;

        var settings = JSON.parse(response.result);

        var whiteDomain = settings.white_domain;
        if (!whiteDomain) return; // when white domain is null, it does not check.

        var existsInvalidDomain = false;

        // NOTE 入力された宛先は複数の箇所に格納されるが、一番取得が容易であった hidden の値を取得する事にしている
        var addresses = document.querySelectorAll('input[name=to], input[name=cc], input[name=bcc]');

        for(var i = 0, addressCount = addresses.length; i < addressCount; i++) {
          var addressValue = addresses[i].value.trim();

          if (!addressValue) return;

          var domain
          if (addressValue.lastIndexOf('>') >= 0) {
            domain = addressValue.match(/@(.*)>$/);
          } else {
            domain = addressValue.match(/@(.*)/);
          }

          if (domain && domain[1].trim() !== whiteDomain) {
            existsInvalidDomain = true;
            break;
          }
        }

        notifyAddressValidatedResult(existsInvalidDomain, settings);
      }); // chrome extension
    }, true); // event listener blur
  }, 100); // setTimeout
})();

function notifyAddressValidatedResult(existsInvalidDomain, settings) {
  var invalidColor = settings.invalid_color || 'red'; // red is default color
  var validColor = settings.valid_color || 'lightblue'; // lightblue is default color

  var changingColor = existsInvalidDomain ? invalidColor : validColor;

  var notifyAreas = document.querySelectorAll('.I5');

  for(var j = 0, notifyAreaCount = notifyAreas.length; j < notifyAreaCount; j++) {
    notifyAreas[j].style.backgroundColor = changingColor;
  }

  // NOTE 返信の場合は .IG のエリアが白で背景色が上書きされているため追加で背景色を設定する
  var notifyAreaInReply = document.querySelectorAll('.IG')
  if (notifyAreaInReply.length > 0) {
    notifyAreaInReply[0].style.backgroundColor = changingColor;
  }
}
