chrome.extension.onRequest.addListener(function(request, sender, sendResponse) { 
  if (request.method == "getLocalStorageData") {
    var result = localStorage[request.key];
    sendResponse({result: result});
  } else {
    sendResponse({});
  }
});
